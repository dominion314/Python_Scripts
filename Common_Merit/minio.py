from minio import Minio

# Initialize minioClient with an endpoint and access/secret keys.
minioClient = Minio('YOUR_SERVICE_NAME',
                  access_key='YOUR_ACCESS_KEY',
                  secret_key='YOUR_SECRET_KEY',
                  secure=True)

# Create a bucket
bucket_name='my-bucket-name' 
minioClient.make_bucket(bucket_name)

# Upload a file
file_name='my-file-name.txt'
minioClient.f