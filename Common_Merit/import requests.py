import requests
import datetime

# Get the current time and date
now = datetime.datetime.now()

# Get the user's location
location = input("Enter your location: ")

# Use the OpenWeatherMap API to get the weather for the user's location
api_key = "50cff2a86a73de8bfa9302daafb67295"
api_url = "https://api.openweathermap.org/data/3.0/onecall?lat={lat}&lon={lon}&exclude={part}&appid={API key}"
response = requests.get(api_url)
data = response.json()

# Extract the weather data from the API response
weather = data["weather"][0]["main"]
temperature = data["main"]["temp"]

# Print the time, weather, and temperature to the console
print("It is currently {} and {} degrees in {}".format(now.strftime("%H:%M"), temperature, location))
print("The weather is:", weather)