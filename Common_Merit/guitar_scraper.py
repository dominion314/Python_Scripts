import requests
from bs4 import BeautifulSoup

# Ask user for artist name
artist_name = input("Enter the artist name: ")

# Form the URL for the artist page
url = f"https://www.ultimate-guitar.com/search.php?search_type=title&value={artist_name}"

# Make a request to the URL
response = requests.get(url)

# Parse the HTML of the page
soup = BeautifulSoup(response.text, "html.parser")

#Find the link of artist page
artist_link = soup.find("a", {"class":"search-result__title"})

if artist_link:
    # Extract the link
    artist_page_link = artist_link["href"]
    print(f"Artist Page Link: {artist_page_link}")
    #Make a request to the artist page
    artist_page_response = requests.get(artist_page_link)
    artist_page_soup = BeautifulSoup(artist_page_response.text, "html.parser")
    #Extract the songs
    song_elements = artist_page_soup.find_all("td", {"class":"song-name"})
    songs = [song.text for song in song_elements]
    print(f"Songs: {songs}")
else:
    print(f"No artist page found for {artist_name}")
