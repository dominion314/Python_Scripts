import os
import hashlib

def hash_file(filename):
   """"This function returns the SHA-1 hash
   of the file passed into it"""

   # make a hash object
   h = hashlib.sha1()

   # open file for reading in binary mode
   with open(filename,'rb') as file:

       # loop till the end of the file
       chunk = 0
       while chunk != b'':
           # read only 1024 bytes at a time
           chunk = file.read(1024)
           h.update(chunk)

   # return the hex representation of digest
   return h.hexdigest()

def scan_files():
    """This function scans all files in the directory and subdirectories,
    and compares their hash with a predefined list of known safe files"""

    # known safe files
    safe_files = {
        "example.txt": "3858f62230ac3c915f300c664312c63f9c84301b",
        "program.exe": "5f5c2a2f5d07c5c5d08c5c5d09c5c5d10c5c5d11"
    }

    # scan all files in directory and subdirectories
    for root, dirs, files in os.walk("."):
        for file in files:
            file_path = os.path.join(root, file)
            file_hash = hash_file(file_path)
            # check if file hash is in the known safe files
            if file_hash in safe_files.values():
                continue
            else:
                print("Unsafe file:", file_path)

scan_files()