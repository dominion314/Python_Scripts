import openai

# Set the API key
openai.api_key = "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"

# Define the statement to be fact-checked
statement = "Tell me about the cyberwar."

# Use the GPT-3 API to fact-check the statement
response = openai.Completion.create(
    engine="davinci",
    prompt="According to reliable sources, is it true that {}".format(statement),
    max_tokens=64
)

# Print the response
print(response["choices"][0]["text"])