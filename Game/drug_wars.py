import random

# Set up the game world
locations = ["Downtown", "Suburbs", "Beach"]
dealers = {"Downtown": {"Cocaine": 100, "Marijuana": 50},
           "Suburbs": {"Cocaine": 80, "Marijuana": 40},
           "Beach": {"Cocaine": 120, "Marijuana": 60}}
player_location = random.choice(locations)
player_money = 1000
player_debt = 0

# Create a loop
while True:
    # Display the game world
    print("Location:", player_location)
    print("Money:", player_money)
    print("Debt:", player_debt)
    print("Prices:")
    for drug, price in dealers[player_location].items():
        print(drug + ":", price)

    # Allow the player to make decisions
    action = input("What do you want to do? (buy, sell, move): ")

    # Update the game world
    if action == "buy":
        drug = input("Which drug do you want to buy? (Cocaine, Marijuana): ")
        if drug in dealers[player_location]:
            price = dealers[player_location][drug]
            if player_money >= price:
                player_money -= price
                print("You bought", drug, "for", price)
            else:
                print("You don't have enough money to buy", drug)
        else:
            print("That drug is not available here.")
    elif action == "sell":
        drug = input("Which drug do you want to sell? (Cocaine, Marijuana): ")
        if drug in dealers[player_location]:
            price = dealers[player_location][drug]
            player_money += price
            print("You sold", drug, "for", price)
        else:
            print("That drug is not available here.")
    elif action == "move":
        player_location = input("Where do you want to go? (Downtown, Suburbs, Beach): ")
        if player_location not in locations:
            print("That location doesn't exist.")
    else:
        print("Invalid action.")

    # End the game if the player has enough money or fails to pay off a debt
    if player_money >= 10000:
        print("You win!")
        break
    elif player_debt >= 1000:
        print("You lose!")
        break
    else:
        # Update the prices of drugs randomly
        for location in dealers:
            for drug in dealers[location]:
                dealers[location][drug] += random.randint(-10, 10)
                if dealers[location][drug] < 0:
                    dealers[location][drug] = 0

        # Increase the player's debt randomly
        if random.random() < 0.1:
            player_debt += random.randint(100, 500)
            print("You took out a loan of", player_debt)
