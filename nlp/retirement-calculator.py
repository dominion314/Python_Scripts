import math

def retire(current_age, retire_age, salary):
    years_until_retirement = retire_age - current_age
    savings_per_year = salary / 10 # assuming you need to save 10% of your salary per year to retire comfortably
    total_savings = savings_per_year * years_until_retirement * 12 # multiply by 12 to convert to monthly savings
    return years_until_retirement, savings_per_year, total_savings

current_age = int(input("Enter your current age: "))
retire_age = int(input("Enter the age at which you want to retire: "))
salary = int(input("Enter your current yearly salary: "))
rate = float(input("Enter the annual interest rate (in percentage): "))
rate = 7.5


years_until_retirement, savings_per_year, total_savings = retire(current_age, retire_age, salary)

print("You have {} years until retirement.".format(years_until_retirement))
print("You need to save ${:,.2f} per year to retire comfortably.".format(savings_per_year))
print("In total, you will need to save ${:,.2f} to retire comfortably.".format(total_savings))
