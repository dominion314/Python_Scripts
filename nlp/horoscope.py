import openai
from datetime import datetime

openai.api_key = "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"

def get_horoscope(month, day):
    """
    Returns the horoscope for the given month and day using GPT-3.
    """
    # Prompt for the horoscope
    prompt = f"What is the horoscope for someone born on {month} {day}?"
    
    # Call the GPT-3 API
    completions = openai.Completion.create(
        engine="davinci",
        prompt=prompt,
        max_tokens=1024,
        temperature=0.5,
    )
    
    # Get the first completion
    message = completions.choices[0].text
    
    return message

def get_birthday_horoscope(birthday):
    """
    Returns the horoscope for the given birthday using GPT-3.
    Birthday should be a string in the format 'MMDDYYYY'.
    """
    # Parse the birthday string
    birthday_dt = datetime.strptime(birthday, '%m%d%Y')
    
    # Get the month and day from the parsed date
    month = birthday_dt.strftime('%B')
    day = birthday_dt.day
    
    # Get the horoscope for the month and day
    horoscope = get_horoscope(month, day)
    
    return horoscope

# Get the birthday from the user
birthday = input("Enter your birthday (MMDDYYYY): ")

# Get the horoscope for the birthday
horoscope = get_birthday_horoscope(birthday)

# Print the horoscope
print(horoscope)
