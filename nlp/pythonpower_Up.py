# ----------------------------------------------Variables and data types

# name = "John"

# name = "Jane"

# name = "John"
# age = "20"

# age = 20
# height = 150

# weight = 50.5

# is_student = True
# is_adult = False

# print(type(name))  # Output: <class 'str'>
# print(type(age))  # Output: <class 'int'>

# age = "20"
# print(type(age)) # Output: <class 'str'>
# age = int(age)
# print(type(age)) # Output: <class 'int'>


# --------------------------------------------------Operators and Expressions

# # Addition
# x = 5 + 2  # x will be 7
# # Subtraction
# y = 8 - 3  # y will be 5
# # Multiplication
# z = 2 * 4  # z will be 8
# # Division
# a = 10 / 2  # a will be 5.0

# # Equal to 
# x = 5
# y = 8
# print(x == y)  # Output: False
# # Not equal to
# print(x != y)  # Output: True
# # Greater than
# print(x > y)  # Output: False
# # Less than
# print(x < y)  # Output: True

# # And operator
# x = True
# y = False
# print(x and y)  # Output: False
# # Or operator
# print(x or y)  # Output: True
# # Not operator
# print(not x)  # Output: False


# # Assign value to a variable
# x = 5
# # Add and assign value
# x += 2  # x will be 7
# # Subtract and assign value
# x -= 1  # x will be 6
# # Multiply and assign value
# x *= 2  # x will be 12
# # Divide and assign value
# x /= 2  # x will be 6.0

# # Without parentheses
# x = 5 + 2 * 4  # x will be 13
# # With parentheses
# x = (5 + 2) * 4  # x will be 24



# -------------------------------------------------------------Control Flow

# x = 5
# if x > 0:
#     print("x is positive")
# else:
#     print("x is not positive")


# fruits = ["apple", "banana", "cherry"]
# for fruit in fruits:
#     print(fruit)


#     x = 0
# while x < 5:
#     print(x)
#     x += 1

# x = 25
# if x < 0:
#     print("x is negative")
# elif x >= 0 and x <= 25:
#     print("x is between 0 and 25")
# else:
#     print("x is greater than 25")


# ------------------------------------------------------Functions and modules

# def add(x, y):
#     return x + y
# result = add(5, 3)
# print(result) # Output: 8


# def count_vowels(string):
#     vowels = "aeiouAEIOU"
#     count = 0
#     for char in string:
#         if char in vowels:
#             count += 1
#     return count

# result = count_vowels("Hello, World!")
# print(result) # Output: 3


# import math

# def area_of_circle(radius):
#     return math.pi * math.pow(radius, 2)

# result = area_of_circle(5)
# print(result) # Output: 78.53981633974483


# ------------------------------------------------------Classes and Objects

# class Car:
#     def __init__(self, make, model, year):
#         self.make = make
#         self.model = model
#         self.year = year




# my_car = Car("Toyota", "Camry", 2020)
# print(my_car.make) # Output: Toyota
# print(my_car.model) # Output: Camry
# print(my_car.year) # Output: 2020



# --------------------------------------------------------------File Handling

# file = open("example.txt", "r")

# content = file.read()
# print(content)

# file = open("example.txt", "w")
# file.write("Hello, World!")

# file.close()

# ----------------------------------------------------Exception Handling
# try:
#     file = open("example.txt", "r")
#     content = file.read()
#     print(content)
# except FileNotFoundError:
#     print("The file does not exist.")


# try:
#     file = open("example.txt", "r")
#     content = file.read()
#     print(content)
# except FileNotFoundError:
#     print("The file does not exist.")
# finally:
#     file.close()


# ---------------------------------------Working with libraries and APIs

# import requests

# response = requests.get("https://www.example.com")
# print(response.text)



# -------------------------------------Projects
# #Calculator

# # This function performs the requested operation
# def calculate(operator, num1, num2):
#     if operator == "+":
#         return num1 + num2
#     elif operator == "-":
#         return num1 - num2
#     elif operator == "*":
#         return num1 * num2
#     elif operator == "/":
#         return num1 / num2
#     else:
#         return "Invalid operator"

# # The program will start here
# if __name__ == "__main__":
#     operator = input("Enter an operator (+, -, *, /): ")
#     num1 = float(input("Enter the first number: "))
#     num2 = float(input("Enter the second number: "))
#     result = calculate(operator, num1, num2)
#     print(result)


# #Guessing Game

# import random

# # Generate a random number between 1 and 100
# answer = random.randint(1, 100)

# # Loop until the player guesses the correct number
# while True:
#     # Get player's guess
#     guess = int(input("Guess a number between 1 and 100: "))

#     # Check if the guess is correct
#     if guess == answer:
#         print("Congratulations! You guessed the correct number.")
#         break
#     # Check if the guess is too high
#     elif guess > answer:
#         print("Too high. Try again.")
#     # The guess must be too low
#     else:
#         print("Too low. Try again.")


# #Guessing game 2
# import random

# # List of words
# words = ["python","javascript","java","c++","c#","swift","kotlin","go","rust"]

# answer = random.choice(words)

# # loop until the player guess the correct word
# while True:
#     guess = input("Guess a programming language : ")
#     if guess.lower() == answer:
#         print("Congratulations! You guessed the correct word.")
#         break
#     else:
#         print("Incorrect! Try again.")



# #Chatbot
# from chatterbot import ChatBot
# from chatterbot.trainers import ChatterBotCorpusTrainer

# # Create a new instance of a ChatBot
# chatbot = ChatBot("Ron Obvious")

# # Create a new trainer for the ChatBot
# trainer = ChatterBotCorpusTrainer(chatbot)

# # Now let us train our bot with multiple corpus
# trainer.train("chatterbot.corpus.english.greetings",
#               "chatterbot.corpus.english.conversations")

# # Get a response to the input text 'How are you?'
# response = chatbot.get_response("How are you?")
# print(response)



# #Chaterbot2

# import nltk
# from nltk.chat.util import Chat, reflections
# pairs = [
#     [
#         r"my name is (.*)",
#         ["Hello %1, How are you today?"]
#     ],
#     [
#         r"hi|hey|hello",
#         ["Hello", "Hey there"]
#     ],
#     [
#         r"what is your name ?",
#         ["I am a chatbot, you can call me whatever you like."]
#     ],
#     [
#         r"how are you ?",
#         ["I'm doing good, how about you?"]
#     ],
#     [
#         r"sorry (.*)",
#         ["Its alright","Its OK, never mind"]
#     ],
#     [
#         r"I am fine",
#         ["Great to hear that! How can I help you today?"]
#     ],
#     [
#         r"quit",
#         ["Bye bye take care. It was nice talking to you :) "]
#     ],
# ]
# chatbot = Chat(pairs, reflections)
# chatbot.converse()



# -------------------------------------Making a website

# from flask import Flask, render_template

# app = Flask(__name__)

# @app.route("/")
# def home():
#     return render_template("home.html")

# if __name__ == "__main__":
#     app.run(debug=True)




# <!DOCTYPE html>
# <html>
# <head>
#     <title>My Website</title>
# </head>
# <body>
#     <h1>Welcome to my website!</h1>
#     <p>This is the home page.</p>
# </body>
# </html>


# --------------------------------------------Web Scraping

# import requests
# from bs4 import BeautifulSoup

# url = "https://www.example.com"
# response = requests.get(url)
# soup = BeautifulSoup(response.text, "html.parser")

# title = soup.find("title").get_text()
# print("Title:", title)

# Title: example.com