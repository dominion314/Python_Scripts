import requests
import json

ALPHA_VANTAGE_API_KEY = '6RW98Q1QL1YPZVIT'

def get_financial_data(ticker):
    # Request company overview data from Alpha Vantage
    url = f'https://www.alphavantage.co/query?function=OVERVIEW&symbol={ticker}&apikey={ALPHA_VANTAGE_API_KEY}'
    response = requests.get(url)
    data = json.loads(response.text)

    # Example: Print some financial data
    print(f"Ticker: {ticker}")
    print(f"P/E Ratio (TTM): {data['PERatio']}")  # Note: This is trailing twelve months (TTM) P/E ratio, not 5-year P/E ratio
    print(f"Revenue Growth (TTM): ${float(data['RevenueTTM']):,.0f}")  # Note: This is trailing twelve months (TTM) revenue
    print(f"Gross Profit (TTM): ${float(data['GrossProfitTTM']):,.0f}")  # Note: This is trailing twelve months (TTM) gross profit
    print(f"Shares Outstanding: {int(data['SharesOutstanding']):,}")

    # Request cash flow statement from Alpha Vantage
    url = f'https://www.alphavantage.co/query?function=CASH_FLOW&symbol={ticker}&apikey={ALPHA_VANTAGE_API_KEY}'
    response = requests.get(url)
    cash_flow_data = json.loads(response.text)['annualReports']

    # Calculate 5-year cash flow growth
    try:
        cash_flow_5yr_ago = float(cash_flow_data[4]['operatingCashflow'])
        cash_flow_latest = float(cash_flow_data[0]['operatingCashflow'])
        cash_flow_growth_5yr = ((cash_flow_latest / cash_flow_5yr_ago) - 1) * 100
        print(f"Cash Flow Growth 5 YR: {cash_flow_growth_5yr:.2f}%")
    except (IndexError, KeyError, ValueError):
        print("Cash Flow Growth 5 YR: Not available")

# Replace 'AAPL' with the desired stock ticker
get_financial_data('AAPL')
