from django.shortcuts import render
from django.http import HttpResponse
from .pokertrainer import calculate_win_probabilities

def index(request):
    if request.method == 'POST':
        hole_cards = [request.POST.get('hole_card_1'), request.POST.get('hole_card_2')]
        community_cards = [request.POST.get('flop_1'), request.POST.get('flop_2'), request.POST.get('flop_3'),
                           request.POST.get('turn'), request.POST.get('river')]
        num_players = int(request.POST.get('num_players'))
        num_simulations = int(request.POST.get('num_simulations'))

        win_prob = calculate_win_probabilities(hole_cards, community_cards, num_players, num_simulations)

        return render(request, 'results.html', {'win_prob': win_prob})

    return render(request, 'index.html')
