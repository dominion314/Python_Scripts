from django.shortcuts import render
from .pokertrainer import simulate_hand, calculate_win_probabilities

# your view functions here


import random

def deal(num_players):
    deck = ['2h', '3h', '4h', '5h', '6h', '7h', '8h', '9h', 'Th', 'Jh', 'Qh', 'Kh', 'Ah',
            '2d', '3d', '4d', '5d', '6d', '7d', '8d', '9d', 'Td', 'Jd', 'Qd', 'Kd', 'Ad',
            '2c', '3c', '4c', '5c', '6c', '7c', '8c', '9c', 'Tc', 'Jc', 'Qc', 'Kc', 'Ac',
            '2s', '3s', '4s', '5s', '6s', '7s', '8s', '9s', 'Ts', 'Js', 'Qs', 'Ks', 'As']
    random.shuffle(deck)
    hands = [deck[i::num_players] for i in range(num_players)]
    return hands

def evaluate(hand, community_cards):
    # Add the hole cards and community cards together
    all_cards = hand + community_cards

    # Create a dictionary of card values and counts
    counts = {}
    for card in all_cards:
        value = card[0]
        counts[value] = counts.get(value, 0) + 1

    # Check for flushes
    flush_suits = [card[1] for card in all_cards if counts[card[0]] >= 5]
    if flush_suits:
        for suit in flush_suits:
            flush_cards = [card for card in all_cards if card[1] == suit]
            if len(flush_cards) >= 5:
                values = sorted([int(card[0].replace('T', '10').replace('J', '11').replace('Q', '12').replace('K', '13').replace('A', '14')) for card in flush_cards], reverse=True)
                if values[0] == 14 and values[-1] == 10:
                    return 'royal_flush'
                elif values[0] - values[-1] == 4:
                    return 'straight_flush'
                else:
                    return 'flush'

    # Check for straights
    unique_values = list(set([int(card[0].replace('T', '10').replace('J', '11').replace('Q', '12').replace('K', '13').replace('A', '14')) for card in all_cards]))
    unique_values.sort(reverse=True)
    straight_values = []
    for i in range(len(unique_values) - 4):
        if unique_values[i] - unique_values[i+4] == 4:
            straight_values = unique_values[i:i+5]
            break
    if straight_values:
        if straight_values[0] == 14 and straight_values[-1] == 10:
            return 'royal_flush'
        else:
            return 'straight'

    # Check for pairs, three of a kind, and a four of a kind
    pairs = []
    three_of_a_kind = []
    four_of_a_kind = []
    for value in counts:
        if counts[value] == 2:
            pairs.append(value)
        elif counts[value] == 3:
            three_of_a_kind.append(value)
        elif counts[value] == 4:
            four_of_a_kind.append(value)
    
    if four_of_a_kind:
        return 'four_of_a_kind'
    elif len(three_of_a_kind) == 1 and len(pairs) == 1:
        return 'full_house'
    elif len(three_of_a_kind) == 1:
        return 'three_of_a_kind'
    elif len(pairs) == 2:
        return 'two_pairs'
    elif len(pairs) == 1:
        return 'pair'

    # If no hand was found, return the highest card
    values = sorted([int(card[0].replace('T', '10').replace('J', '11').replace('Q', '12').replace('K', '13').replace('A', '14')) for card in all_cards], reverse=True)
    return str(values[0])


def simulate_hand(hole_cards, community_cards, num_players):
    all_cards = hole_cards + community_cards
    player_hand = []
    for card in all_cards:
        if card not in player_hand:
            player_hand.append(card)
    num_community_cards = len(community_cards)

    for i in range(num_community_cards, 5):
        community_cards.append(deal(1)[0][0])

    values = [evaluate(player_hand[:2], community_cards) for j in range(num_players)]
    max_value = max(values)
    if values.count(max_value) == 1:
        return values.index(max_value) + 1, max_value
    else:
        max_indexes = [i for i in range(num_players) if values[i] == max_value]
        if len(max_indexes) == 2:
            for i in range(2):
                community_cards.append(deal(1)[0][0])
                values = [evaluate(player_hand[:2], community_cards) for j in max_indexes]
                max_value = max(values)
                if values.count(max_value) == 1:
                    return max_indexes[values.index(max_value)] + 1, max_value
            return 0, max_value
        else:
            return 0, max_value
            
def calculate_win_probabilities(hole_cards, community_cards, num_players, num_simulations):
    wins = 0
    for i in range(num_simulations):
        deck = ['2h', '3h', '4h', '5h', '6h', '7h', '8h', '9h', 'Th', 'Jh', 'Qh', 'Kh', 'Ah',
                '2d', '3d', '4d', '5d', '6d', '7d', '8d', '9d', 'Td', 'Jd', 'Qd', 'Kd', 'Ad',
                '2c', '3c', '4c', '5c', '6c', '7c', '8c', '9c', 'Tc', 'Jc', 'Qc', 'Kc', 'Ac',
                '2s', '3s', '4s', '5s', '6s', '7s', '8s', '9s', 'Ts', 'Js', 'Qs', 'Ks', 'As']
        for card in hole_cards:
            deck.remove(card)
        for card in community_cards:
            deck.remove(card)
        for j in range(num_players-1):
            opp_hole_cards = deal(2)
            for card in opp_hole_cards:
                deck.remove(card)
            opp_win_prob = calculate_win_probabilities(opp_hole_cards, community_cards, 1, num_simulations)
            if opp_win_prob >= 0.5:
                break
        else:
            winner, hand_type = simulate_hand(hole_cards, community_cards, num_players)
            if winner == 1:
                wins += 1
    win_prob = wins / num_simulations
    return win_prob


