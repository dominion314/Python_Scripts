import requests
import json
from datetime import datetime, timedelta

ALPHA_VANTAGE_API_KEY = '6RW98Q1QL1YPZVIT'

def get_historical_data(ticker):
    url = f"https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol={ticker}&outputsize=full&apikey={ALPHA_VANTAGE_API_KEY}"
    response = requests.get(url)
    data = json.loads(response.text)
    time_series_data = data['Time Series (Daily)']

    # Calculate 5 years ago
    now = datetime.now()
    five_years_ago = now - timedelta(days=5 * 365)

    filtered_data = {}
    for date, daily_data in time_series_data.items():
        if datetime.strptime(date, '%Y-%m-%d') >= five_years_ago:
            filtered_data[date] = daily_data

    return filtered_data

def get_sma_data(ticker):
    url = f"https://www.alphavantage.co/query?function=SMA&symbol={ticker}&interval=daily&time_period=50&series_type=close&apikey={ALPHA_VANTAGE_API_KEY}"
    response = requests.get(url)
    data = json.loads(response.text)
    time_series_data = data['Technical Analysis: SMA']

    # Calculate 5 years ago
    now = datetime.now()
    five_years_ago = now - timedelta(days=5 * 365)

    filtered_data = {}
    for date, daily_data in time_series_data.items():
        if datetime.strptime(date, '%Y-%m-%d') >= five_years_ago:
            filtered_data[date] = daily_data

    return filtered_data

ticker = 'AAPL'
historical_data = get_historical_data(ticker)
sma_data = get_sma_data(ticker)

print(f"Adjusted Close Prices for {ticker} (Past 5 Years):")
print(json.dumps(historical_data, indent=2))

print(f"\nSimple Moving Averages for {ticker} (Past 5 Years):")
print(json.dumps(sma_data, indent=2))
