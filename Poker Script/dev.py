import tkinter as tk

def on_submit():
    hole_cards = [entry1.get(), entry2.get()]
    community_cards = [entry3.get(), entry4.get(), entry5.get(), entry6.get(), entry7.get()]
    num_players = int(entry8.get())
    num_simulations = int(entry9.get())

    win_prob = calculate_win_probabilities(hole_cards, community_cards, num_players, num_simulations)

    result_label.config(text='Win probability: {:.2f}%'.format(win_prob * 100))

# Create the window and labels
window = tk.Tk()
window.title('Poker Calculator')

label1 = tk.Label(window, text='Hole Card 1:')
label1.grid(row=0, column=0)
entry1 = tk.Entry(window)
entry1.grid(row=0, column=1)

label2 = tk.Label(window, text='Hole Card 2:')
label2.grid(row=1, column=0)
entry2 = tk.Entry(window)
entry2.grid(row=1, column=1)

label3 = tk.Label(window, text='Flop 1:')
label3.grid(row=2, column=0)
entry3 = tk.Entry(window)
entry3.grid(row=2, column=1)

label4 = tk.Label(window, text='Flop 2:')
label4.grid(row=3, column=0)
entry4 = tk.Entry(window)
entry4.grid(row=3, column=1)

label5 = tk.Label(window, text='Flop 3:')
label5.grid(row=4, column=0)
entry5 = tk.Entry(window)
entry5.grid(row=4, column=1)

label6 = tk.Label(window, text='Turn:')
label6.grid(row=5, column=0)
entry6 = tk.Entry(window)
entry6.grid(row=5, column=1)

label7 = tk.Label(window, text='River:')
label7.grid(row=6, column=0)
entry7 = tk.Entry(window)
entry7.grid(row=6, column=1)

label8 = tk.Label(window, text='Number of players:')
label8.grid(row=7, column=0)
entry8 = tk.Entry(window)
entry8.grid(row=7, column=1)

label9 = tk.Label(window, text='Number of simulations:')
label9.grid(row=8, column=0)
entry9 = tk.Entry(window)
entry9.grid(row=8, column=1)

result_label = tk.Label(window, text='')
result_label.grid(row=9, column=0, columnspan=2)

# Create the submit button
submit_button = tk.Button(window, text='Calculate', command=on_submit)
submit_button.grid(row=10, column=0, columnspan=2)

# Start the event loop
window.mainloop()
