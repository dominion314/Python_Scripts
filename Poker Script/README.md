    Assign values to each card based on its rank (Ace = 14, King = 13, Queen = 12, Jack = 11, 10-2 = 10-2). This will be used later to determine the strength of the hand.

    Create a function that takes in an array of cards and returns the strength of the hand. You can use the following rankings to determine the hand strength:

    Royal flush: Ace, King, Queen, Jack, and 10 of the same suit (highest ranking)
    Straight flush: any five cards of the same suit in numerical order
    Four of a kind: four cards of the same rank
    Full house: three cards of one rank and two cards of another rank
    Flush: any five cards of the same suit
    Straight: any five cards of sequential rank, not all of the same suit
    Three of a kind: three cards of the same rank
    Two pair: two cards of one rank, two cards of another rank
    One pair: two cards of the same rank
    High card: the highest ranking card in the hand (lowest ranking)

    Sort the array of cards in descending order based on their assigned values.

    Check for royal flush, straight flush, four of a kind, full house, flush, and straight in that order. If the hand meets any of these criteria, return the appropriate hand strength.

    If the hand does not meet any of the above criteria, check for three of a kind, two pair, and one pair in that order. If the hand meets any of these criteria, return the appropriate hand strength.

    If the hand does not meet any of the above criteria, return the high card.