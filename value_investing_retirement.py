# Prompt the user to enter the required values
P = float(input("Enter the initial investment amount: "))
C = float(input("Enter the regular contribution amount: "))
n = int(input("Enter the number of years for the investment: "))
r = float(input("Enter the annual interest rate (as a decimal): "))

# Calculate the future value of the investment using the formula
FV = P * ((1 + r)**n - 1) / r + C * ((1 + r)**n - 1) / r

# Display the future value to the user
print("The future value of your investment is: $" + "{:.2f}".format(FV))
