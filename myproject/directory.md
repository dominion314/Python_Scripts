  myproject/  
    myproject/
    ├── pokertrainer/
    ├──---templates/
    │     ├── cards.html
    │     ├── choose_cards.html
    │     └── index.html
    ├── __init__.py
    ├── admin.py
    ├── apps.py
    ├── models.py
    ├── tests.py
    └── views.py
    └── wsgi.py
    └── signals.py
    └── urls.py
    ├── settings.py
    ├── static/
    │   ├── cards/
    │   │   ├── ace_of_spades.png
    │   │   ├── ace_of_hearts.png
    │   │   ├── ace_of_diamonds.png
    │   │   └── ace_of_clubs.png
    │   └── css/
    │       └── style.css