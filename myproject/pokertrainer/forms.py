def choose_cards(request):
    # Get all available cards from the database
    cards = Card.objects.all()

    # Pass the cards to the template as context data
    context = {
        'cards': cards
    }

    # Render the template with the context data
    return render(request, 'choose_cards.html', context)
