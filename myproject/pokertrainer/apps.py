from django.apps import AppConfig

class PokerTrainerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'pokertrainer'

    def ready(self):
        import pokertrainer.signals

