from django.core.management.base import BaseCommand
from django.db.models import Count
from pokertrainer.models import Card

class Command(BaseCommand):
    help = 'Removes duplicate cards from the database'

    def handle(self, *args, **options):
        # Assuming your Card model has a field named 'value' to identify the card
        duplicates = Card.objects.values('name').annotate(count=Count('name')).filter(count__gt=1)

        for duplicate in duplicates:
            cards_to_delete = Card.objects.filter(name=duplicate['name'])[1:]
            cards_to_delete.delete()


        self.stdout.write(self.style.SUCCESS('Duplicates removed successfully.'))

