from django.apps import AppConfig
from django.apps import apps
from django.db.models.signals import post_migrate

def create_cards(sender, **kwargs):
    if sender.name == 'pokertrainer':
        Card = apps.get_model('pokertrainer', 'Card')
        cards = ['Ace of Spades', 'Ace of Hearts', 'Ace of Diamonds', 'Ace of Clubs']
        for card in cards:
            Card.objects.get_or_create(name=card)

class PokerTrainerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'pokertrainer'

    def ready(self):
        post_migrate.connect(create_cards, sender=self)
