from django.shortcuts import render
from pokertrainer.models import Card

def index(request):
    # check if there are any cards in the database
    if Card.objects.count() == 0:
        # create and save new cards
        Card.objects.create(name='Ace of Spades', image='ace_of_spades.png')
        Card.objects.create(name='Ace of Hearts', image='ace_of_hearts.png')
        Card.objects.create(name='Ace of Diamonds', image='ace_of_diamonds.png')
        Card.objects.create(name='Ace of Clubs', image='ace_of_clubs.png')
    
    # get all the cards from the database
    cards = Card.objects.all()

    context = {
        'cards': cards,
    }
    return render(request, 'index.html', context)

def game(request):
    # Get the user's selected cards and community cards from the request
    card1_id = request.POST['card1']
    card2_id = request.POST['card2']
    community_card_ids = request.POST.getlist('community_cards')

    # Get the cards from the database
    card1 = Card.objects.get(id=card1_id)
    card2 = Card.objects.get(id=card2_id)
    community_cards = Card.objects.filter(id__in=community_card_ids)

    context = {
        'card1': card1,
        'card2': card2,
        'community_cards': community_cards,
    }

    return render(request, 'game.html', context)

def choose_cards(request):
    if request.method == 'POST':
        # Get the user's selected cards from the request
        card1_id = request.POST['card1']
        card2_id = request.POST['card2']

        # Get the cards from the database
        card1 = Card.objects.get(id=card1_id)
        card2 = Card.objects.get(id=card2_id)

        # Get the first three community cards from the dealer
        community_cards = Card.objects.order_by('?')[:3]

        context = {
            'card1': card1,
            'card2': card2,
            'community_cards': community_cards,
        }

        return render(request, 'game.html', context)
    else:
        # Get all available cards from the database
        cards = Card.objects.all()

        # Pass the cards to the template as context data
        context = {
            'cards': cards
        }

        # Render the template with the context data
        return render(request, 'choose_cards.html', context)
